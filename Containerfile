FROM ubuntu:20.04
MAINTAINER Kris Chaplin <kris.chaplin@amd.com>
# Modified from Cliff Brake's Ubuntu container  <cbrake@bec-systems.com>

ARG DEBIAN_FRONTEND=noninteractive

RUN \
        dpkg --add-architecture arm64 && \
        apt-get update 

RUN apt-get install -yq sudo build-essential git nano vim
RUN           apt-get install -yq python3-yaml libncursesw5 
RUN           apt-get install -yq python python3 man bash diffstat gawk chrpath wget cpio 
RUN           apt-get install -yq texinfo lzop apt-utils bc screen libncurses5-dev locales 
RUN           apt-get install -yq libc6-dev doxygen libssl-dev dos2unix xvfb x11-utils 
RUN           apt-get install -yq libssl-dev:arm64 zlib1g-dev:arm64 
RUN           apt-get install -yq libtool libtool-bin procps python3-distutils pigz socat 
RUN           apt-get install -yq zstd iproute2 lz4 iputils-ping 
RUN           apt-get install -yq curl libtinfo5 net-tools xterm rsync u-boot-tools unzip zip 

RUN        rm -rf /var/lib/apt-lists/* && \
              echo "dash dash/sh boolean false" | debconf-set-selections && \
              dpkg-reconfigure dash

RUN curl https://storage.googleapis.com/git-repo-downloads/repo > /bin/repo && chmod a+x /bin/repo
RUN groupadd build -g 1000
RUN useradd -ms /bin/bash -p build build -u 1028 -g 1000 && \
        usermod -aG sudo build && \
        echo "build:build" | chpasswd

RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen

ENV LANG en_US.utf8

USER build
WORKDIR /home/build
RUN git config --global user.email "build@example.com" && git config --global user.name "Build"

RUN echo "export BB_ENV_PASSTHROUGH_ADDITIONS=\"DL_DIR SSTATE_DIR\"" >> ~/.bashrc
RUN echo "export DL_DIR=\"${HOME}/sstate-cache\"" >> ~/.bashrc
RUN echo "export SSTATE_DIR=\"${HOME}/downloads\"" >> ~/.bashrc
