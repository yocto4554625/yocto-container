podman run \
    --cap-add NET_ADMIN \
    --hostname buildserver \
    -it \
    -v tftpboot:/tftpboot \
    -v `pwd`:/home/build/work \
    -v `pwd`/sstate-cache:/home/build/sstate-cache \
    -v `pwd`/downloads:/home/build/downloads \
    yoctocontainer
